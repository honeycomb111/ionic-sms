webpackJsonp([0],{

/***/ 143:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 143;

/***/ }),

/***/ 185:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 185;

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__ = __webpack_require__(230);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HomePage = (function () {
    // smsdata: AngularFireList<any>;
    function HomePage(db, fb, navCtrl, afDatabase) {
        this.db = db;
        this.fb = fb;
        this.navCtrl = navCtrl;
        // this.smsdata = afDatabase.list('/smsdata').valueChanges();
    }
    HomePage.prototype.sendSMS = function () {
    };
    HomePage.prototype.ngOnInit = function () {
        this.order = this.db.object('/orders/testNumber123');
        this.user = new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["c" /* FormGroup */]({
            phoneNumber: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].minLength(10)]),
            smsTxt: new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].minLength(2)])
        });
    };
    // ngOnInit() {
    //   this.buildForm()
    //   this.order = this.db.object('/orders/testNumber123')
    // }
    HomePage.prototype.updatePhoneNumber = function () {
        this.order.update({ phoneNumber: '+91' + this.phoneNumber, messageUser: this.smsTxt });
        // this.order.update({ phoneNumber: '+919509405590' })
        // this.order.update({ phoneNumber: '+919784038400' })
    };
    HomePage.prototype.buildForm = function () {
        this.numberForm = this.fb.group({
            country: this.validateMinMax(1, 2),
            area: this.validateMinMax(3, 3),
            prefix: this.validateMinMax(3, 3),
            line: this.validateMinMax(4, 4)
        });
    };
    /// helper to add validations to form based on min/max length
    HomePage.prototype.validateMinMax = function (min, max) {
        return ['', [
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].minLength(min),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].maxLength(max),
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].pattern('[0-9]+') // validates input is digit
            ]];
    };
    Object.defineProperty(HomePage.prototype, "e164", {
        get: function () {
            var form = this.numberForm.value;
            var num = form.country + form.area + form.prefix + form.line;
            return "+" + num;
        },
        enumerable: true,
        configurable: true
    });
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/dbws/IonicProjects/MyIonicPro/ionic-sms/ionic-smsApp/src/pages/home/home.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>\n            Ionic SMS\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n    <form [formGroup]="user">\n        <ion-list no-lines>\n\n            <div class="loginDiv">\n                <ion-item class="item">\n                    <ion-input ngModel [(ngModel)]="phoneNumber" formControlName="phoneNumber" type="number" placeholder="Please Enter your phone number"></ion-input>\n                </ion-item>\n            </div>\n            <div class="loginDiv">\n                <ion-item>\n                    <ion-input ngModel [(ngModel)]="smsTxt" formControlName="smsTxt" type="text" placeholder="Enter message"></ion-input>\n                </ion-item>\n            </div>\n\n            <div class="btn">\n                <!-- <button class="btnnn" [disabled]="user.invalid" (click)="sendSMS()">lOGIN</button> -->\n                <!-- <button ion-button [disabled]="user.invalid" (click)="sendSMS()">Send SMS</button> -->\n\n                <button ion-button [disabled]="user.invalid" (click)="updatePhoneNumber()">Send SMS</button>\n            </div>\n\n        </ion-list>\n    </form>\n\n    <!-- <h1>Order Status:\n        <span class="tag is-large" [ngClass]="{\n                        \'is-dark\'    : pizza.status == \'submitted\' ,\n                        \'is-warning\' : pizza.status == \'cooking\',\n                        \'is-primary\' : pizza.status == \'on its way\',\n                        \'is-success\' : pizza.status == \'delivered\'\n                        }">\n          \n          {{ pizza.status }}\n        </span>\n    </h1>\n\n    <ul>\n        <li>Order Number: {{ pizza.$key }}</li>\n        <li>Topping: {{ pizza.topping }}</li>\n        <li>Price: {{ pizza.price }}</li>\n    </ul>\n\n    <hr>\n    <h1>Get Updates via Text Message</h1> -->\n\n    <!-- <form [formGroup]="numberForm" (ngSubmit)="updatePhoneNumber()" novalidate>\n                    <input type="text" formControlName="country" placeholder="1">\n                    <input type="text" formControlName="area" placeholder="916">\n                    <input type="text" formControlName="prefix" placeholder="555">\n                    <input type="text" formControlName="line" placeholder="5555">\n\n                    <input type="submit" value="Get SMS Updates">\n                </form>\n\n                <p *ngIf="numberForm.invalid && numberForm.touched">That\'s not a valid phone number</p> -->\n\n\n    <!-- <h3> Updates will be texed to {{ pizza.phoneNumber || \'none\' }}</h3> -->\n\n</ion-content>'/*ion-inline-end:"/home/dbws/IonicProjects/MyIonicPro/ionic-sms/ionic-smsApp/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_angularfire2_database__["a" /* AngularFireDatabase */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 275:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(276);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(296);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 296:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(346);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_forms__ = __webpack_require__(19);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angularfire2__ = __webpack_require__(65);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










// AF2 Settings
var firebaseConfig = {
    // apiKey: "AIzaSyDW_hS-CUQp1xwtCw-gqXjUC3lEGfMUDBk",
    // authDomain: "ionsms-c090c.firebaseapp.com",
    // databaseURL: "https://ionsms-c090c.firebaseio.com",
    // projectId: "ionsms-c090c",
    // storageBucket: "ionsms-c090c.appspot.com",
    // messagingSenderId: "642913068404"
    apiKey: "AIzaSyBlNbLlE2Gxg6ngtbAL2P5L1QR-M_V_3eo",
    authDomain: "davidsmsapp-ea3d4.firebaseapp.com",
    databaseURL: "https://davidsmsapp-ea3d4.firebaseio.com",
    projectId: "davidsmsapp-ea3d4",
    storageBucket: "davidsmsapp-ea3d4.appspot.com",
    messagingSenderId: "916031799940"
};
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_7__angular_forms__["g" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_9_angularfire2__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__["b" /* AngularFireDatabaseModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_8_angularfire2_database__["b" /* AngularFireDatabaseModule */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 346:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(229);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/dbws/IonicProjects/MyIonicPro/ionic-sms/ionic-smsApp/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/dbws/IonicProjects/MyIonicPro/ionic-sms/ionic-smsApp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[275]);
//# sourceMappingURL=main.js.map