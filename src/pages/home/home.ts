import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';


// const functions = require('firebase-functions');
// const admin = require('firebase-admin');


// const twilio = require('twilio');
// const accountSid = 'AC4398c5f25522e746f170f656af4d5f53';
// const authToken = '726d4fb25e3b54253ccadfd8debd12c5'

// const client = new twilio(accountSid, authToken);



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  phoneNumber: string;
  smsTxt: string;
  user: FormGroup;

  numberForm: FormGroup;
  order: any

  // smsdata: AngularFireList<any>;
  constructor(private db: AngularFireDatabase, private fb: FormBuilder, public navCtrl: NavController, afDatabase: AngularFireDatabase) {

    this.phoneNumber;
    this.smsTxt;
    // this.smsdata = afDatabase.list('/smsdata').valueChanges();
  }

  ngOnInit() {

    this.order = this.db.object('/orders/testNumber123');

    this.user = new FormGroup({

      phoneNumber: new FormControl('', [Validators.required, Validators.minLength(10)]),
      smsTxt: new FormControl('', [Validators.required, Validators.minLength(1)])

    });
  }

  // ngOnInit() {
  //   this.buildForm()
  //   this.order = this.db.object('/orders/testNumber123')
  // }

  updatePhoneNumber() {

    this.order.update({ phoneNumber: '+1' + this.phoneNumber, messageUser: this.smsTxt });

    // this.order = this.db.object('/orders/testNumber123').valueChanges().subscribe((data) => {

    //   console.log("Data---->"+JSON.stringify(data));
    //   // this.pizza = data;
    // });


    // this.ngOnInit();
    // this.order.update({ phoneNumber: '+919509405590' })
    // this.order.update({ phoneNumber: '+919784038400' })
  }

  buildForm() {
    this.numberForm = this.fb.group({
      country: this.validateMinMax(1, 2),
      area: this.validateMinMax(3, 3),
      prefix: this.validateMinMax(3, 3),
      line: this.validateMinMax(4, 4)
    });
  }

  /// helper to add validations to form based on min/max length
  validateMinMax(min, max) {
    return ['', [
      Validators.required,
      Validators.minLength(min),
      Validators.maxLength(max),
      Validators.pattern('[0-9]+')  // validates input is digit
    ]]
  }

  get e164() {
    const form = this.numberForm.value
    const num = form.country + form.area + form.prefix + form.line
    return `+${num}`
  }

}
